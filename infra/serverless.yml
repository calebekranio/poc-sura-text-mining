service: sura-infra

frameworkVersion: '1.51.0'

plugins:
  - serverless-python-requirements

custom:
  prefix: ${self:service}-${self:provider.stage}
  tags: ${file(../tags.yml)}

provider:
  name: aws
  stage: ${opt:stage, 'qa'}
  region: us-east-1
  runtime: python3.7
  stackTags: ${self:custom.tags}
  iamRoleStatements:
    - Effect: Allow
      Action:
        - comprehend:*
        - glue:*
      Resource: '*'

functions:
  createClusterEndpoint:
    handler: functions/comprehend_cluster_endpoint.create
    timeout: 900
  deleteClusterEndpoint:
    handler: functions/comprehend_cluster_endpoint.delete
    timeout: 900
  crawler:
    handler: functions/crawler.handler
    timeout: 900
    environment:
      CRAWLER: !Ref SuraCrawler

resources:
  Resources:
    S3SuraBucket:
      Type: AWS::S3::Bucket
      Properties:
        BucketName: sura-text-mining-poc
        BucketEncryption:
          ServerSideEncryptionConfiguration:
            - ServerSideEncryptionByDefault:
                SSEAlgorithm: aws:kms
        VersioningConfiguration:
          Status: Enabled

    SuraCrawlerRole:
      Type: AWS::IAM::Role
      Properties:
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            -
              Effect: Allow
              Principal:
                Service:
                  - glue.amazonaws.com
              Action:
                - sts:AssumeRole
        Path: /
        Policies:
          -
            PolicyName: root
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                -
                  Effect: Allow
                  Action: '*'
                  Resource: '*'

    SuraDatabase:
      Type: AWS::Glue::Database
      Properties:
        CatalogId: !Ref AWS::AccountId
        DatabaseInput:
          Name: sura_poc_db
          Description: PoC Sura

    SuraCrawler:
      Type: AWS::Glue::Crawler
      Properties:
        Name: sura-poc-crawler
        Role: !GetAtt SuraCrawlerRole.Arn
        DatabaseName: !Ref SuraDatabase
        Targets:
          S3Targets:
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/word-cloud']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/complaints-standard']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/complaints-priority']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/clustered-mails']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/sentiment-analysis']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/tickets-by-sentiments']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/tickets-by-city-state-sentiments']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/text-quality-score-with-comprehend']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/complaints-with-age']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/complaints-count-by-age']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/ranking-regions-complaints']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'enriched/complaints-without-multilines']]
            - Path: !Join ['/', [s3:/, !Ref S3SuraBucket, 'raw/ratings']]
        SchemaChangePolicy:
          UpdateBehavior: LOG
          DeleteBehavior: LOG
